﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    using System.IO;

    //
    // Сводка:
    //     Описывает класс, содержащий методы, реализующих считывание
    //     объекта List<T> из текстового и бинарного файла. 
    public class ACollectionReader : CollectionReader<A>
    {
        //
        // Сводка:
        //     Реализует чтение объекта Project.A из текстового файла.
        // 
        // Параметры:
        //   reader:
        //     Объект, осуществляющий чтение из файла.
        //
        // Возврат:
        //     Объект, считанный из файла.
        //
        // Исключения:
        //   [new] T:System.ArgumentNullException:
        //     Параметр reader имеет значение null.
        //     - Source:
        //       Код ошибки: "-1".
        //
        //   [new] T:System.IO.InvalidDataException:
        //     Не соблюдается необходимое форматирование. Например, значение числового поля представляется строкой, 
        //     содержащей символы, отличные от числовых. Исключение также может возникнуть, если допущена пустая строка.
        //     - Source:
        //       "x":[0..3] - номер строки, на которой возникло исключение.        
        //
        //   [chained] T:System.IO.IOException:
        //     Ошибка считывания. Поток закрыт или достигнут конец потока.
        //     - Source:
        //       "x":[0..3] - номер строки, на которой возникло исключение.
        //
        //   [chained] T:System.InvalidCastException:
        //     Возникает в случае возникновения исключения в конструкторе Project.А().
        //     - Source:
        //       "x":[1..3] - номер свойства объекта Project.A, в set-методе для которого возникло исключение.
        //
        public override A ReadItem(StreamReader reader)
        {
            if (reader == null)
                throw new ArgumentNullException("Параметр reader имеет значение null.") { Source = "-1" };

            string res;
            int temp;
            bool IsParseSuccesful;

            A ST;
            string name;
            int years;
            char gender;

            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считываения.", ex) { Source = "0" };
            }

            if (!String.IsNullOrWhiteSpace(res))
                throw new InvalidDataException("Блоки элементов должны разделяться пустыми строками.") { Source = "0" };

            //NAME
            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считывания.", ex) { Source = "1" };
            }
            if (String.IsNullOrWhiteSpace(res))
                throw new InvalidDataException("Пустая строка.") { Source = "1" };
            name = res.Trim();

            //YEARS
            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считывания.", ex) { Source = "2" };
            }
            IsParseSuccesful = int.TryParse(res.Trim(), out temp);
            if (!IsParseSuccesful)
                throw new InvalidDataException("Недопустимые символы в строке или пустая строка.") { Source = "2" };
            else years = temp;

            //GENDER
            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считывания.", ex) { Source = "3" };
            }
            res.Trim();
            if (String.IsNullOrWhiteSpace(res))
                throw new InvalidDataException("Пустая строка.") { Source = "3" };
            if (res.Length == 1)
                gender = res[0];
            else throw new InvalidDataException("Недопустимые символы в строке.") { Source = "3" };

            try
            {
                ST = new A(name, years, gender);
            }
            catch (Exception e)
            {
                throw new InvalidCastException("Ошибка при создании записи: Недопустимые значения полей", e) { Source = e.Source };
            }

            return ST;
        }
        //
        // Сводка:
        //     Реализует чтение объекта Project.A из бинарного файла.
        //
        // Параметры:
        //   reader:
        //     Объект, осуществляющий чтение из файла.
        //
        // Возврат:
        //     Объект, считанный из файла.
        //
        // Исключения:
        //   [new] T:System.ArgumentNullException:
        //     Параметр reader имеет значение null.
        //     - Source:
        //       Код ошибки: "-1".
        //
        //   [chained] T:System.IO.IOException:
        //     Ошибка считывания. Поток закрыт или достигнут конец потока.
        //     - Source:
        //       "x":[1..3] - номер итерации, на которой возникло исключение.
        //
        //   [chained] T:System.InvalidCastException:
        //     Возникает в случае возникновения исключения в конструкторе Project.А().
        //     - Source:
        //       "x":[1..3] - номер свойства объекта Project.A, в set-методе для которого возникло исключение.
        //
        public override A ReadItem(BinaryReader reader)
        {
            if (reader == null)
                throw new ArgumentNullException("Параметр reader имеет значение null.") { Source = "-1" };

            string name;
            int years;
            char gender;
            A ST;

            try
            {
                name = reader.ReadString();
            }
            catch (Exception ex)
            {
                throw new IOException("Не удалось считать строку (убедитесь, что кодировка символов совпадает с utf-8).", ex) { Source = "1" };
            }

            try
            {
                years = reader.ReadInt32();
            }
            catch (Exception ex)
            {
                throw new IOException("Не удалось считать число (32 бит).", ex) { Source = "2" };
            }

            try
            {
                gender = reader.ReadChar();
            }
            catch (Exception ex)
            {
                throw new IOException("Не удалось считать символ  (убедитесь, что кодировка символов совпадает с utf-8).", ex) { Source = "3" };
            }

            try
            {
                ST = new A(name, years, gender);
            }
            catch (Exception e)
            {
                throw new InvalidCastException("Ошибка при создании записи: Недопустимые значения полей", e) { Source = e.Source };
            }

            return ST;
        }
    }
}
