﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    using System.IO;

    //
    // Сводка:
    //     Описывает класс, содержащий методы, реализующих считывание
    //     объекта List<B> из текстового и бинарного файла.
    //
    class BCollectionReader : CollectionReader<B>
    {
        //
        // Сводка:
        //     Реализует чтение объекта Project.B из текстового файла.
        // 
        // Параметры:
        //   reader:
        //     Объект, осуществляющий чтение из файла.
        //
        // Возврат:
        //     Объект, считанный из файла.
        //
        // Исключения:
        //   [new] T:System.ArgumentNullException:
        //     Параметр reader имеет значение null.
        //     - Source:
        //       Код ошибки: "-1".
        //
        //   [new] T:System.IO.InvalidDataException:
        //     Возникает в случае, если не соблюдается необходимое форматирование.
        //     Например, значение числового поля представляется строкой, содержащей символы, отличные от числовых.
        //     Исключение также может возникнуть, если допущена пустая строка.
        //     - Source:
        //       "x":[0..2] - номер строки, на которой возникло исключение.        
        //
        //   [chained] T:System.InvalidCastException:
        //     Возникает в случае возникновения исключения в конструкторе Project.B().
        //     - Source:
        //       "x":[1..2] - номер свойства объекта Project.B, в set-методе для которого возникло исключение.
        //
        //   [chained] T:System.IO.IOException:
        //     Ошибка считывания. Поток закрыт или достигнут конец потока.
        //     - Source:
        //       "x":[0..2] - номер строки, на которой возникло исключение.
        //
        public override B ReadItem(StreamReader reader)
        {
            if (reader == null)
                throw new ArgumentNullException("Параметр reader имеет значение null.") { Source = "-1" };

            string res;

            B ST;
            string name;
            string adress;

            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считывания.", ex) { Source = "0" };
            }

            if (!String.IsNullOrWhiteSpace(res))
                throw new InvalidDataException("Блоки элементов должны разделяться пустыми строками.") { Source = "0" };

            //NAME
            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считывания.", ex) { Source = "1" };
            }

            if (String.IsNullOrWhiteSpace(res))
                throw new InvalidDataException("Пустая строка.") { Source = "1" };

            name = res.Trim();

            //ADRESS
            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считывания.", ex) { Source = "2" };
            }
            if (String.IsNullOrWhiteSpace(res))
                throw new InvalidDataException("Пустая строка.") { Source = "2" };

            adress = res.Trim();

            try
            {
                ST = new B(name, adress);
            }
            catch (Exception e)
            {
                throw new InvalidCastException("Ошибка при создании записи: Недопустимые значения полей", e) { Source = e.Source };
            }

            return ST;
        }
        //
        // Сводка:
        //     Реализует чтение объекта Project.B из бинарного файла.
        // 
        // Параметры:
        //   reader:
        //     Объект, осуществляющий чтение из файла.
        //
        // Возврат:
        //     Объект, считанный из файла.
        //
        // Исключения:
        //   [new] T:System.ArgumentNullException:
        //     Параметр reader имеет значение null.
        //     - Source:
        //       Код ошибки: "-1".
        //
        //   [new] T:System.InvalidCastException:
        //     Возникает в случае возникновения исключения в конструкторе Project.B().
        //     - Source:
        //       "x":[1..2] - номер свойства объекта Project.B, в set-методе для которого возникло исключение.
        //
        //   [chained] T:System.IO.IOException:
        //     Ошибка считывания. Поток закрыт или достигнут конец потока.
        //     - Source:
        //       "x":[1..2] - номер итерации, на которой возникло исключение.
        //
        public override B ReadItem(BinaryReader reader)
        {
            if (reader == null)
                throw new ArgumentNullException("Параметр reader имеет значение null.") { Source = "-1" };

            string name;
            string adress;
            B ST;

            try
            {
                name = reader.ReadString();
            }
            catch (Exception ex)
            {
                throw new IOException("Не удалось считать строку (убедитесь, что кодировка символов совпадает с utf-8).", ex) { Source = "1" };
            }

            try
            {
                adress = reader.ReadString();
            }
            catch (Exception ex)
            {
                throw new IOException("Не удалось считать строку (убедитесь, что кодировка символов совпадает с utf-8).", ex) { Source = "2" };
            }

            try
            {
                ST = new B(name, adress);
            }
            catch (Exception e)
            {
                throw new InvalidCastException("Ошибка при создании записи: Недопустимые значения полей", e) { Source = e.Source };
            }

            return ST;
        }
    }
}
