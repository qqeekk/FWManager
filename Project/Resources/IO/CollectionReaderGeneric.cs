﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    using System.Collections.ObjectModel;
    using System.IO;

    //
    // Сводка:
    //     Описывает класс, содержащий методы, реализующих считывание
    //     объекта List<T> из текстового и бинарного файла. 
    //
    public abstract class CollectionReader<T> : IItemReader<T>
    {
        //
        // Сводка:
        //     Реализует считывание объекта T из бинарного файл.
        // 
        // Параметры:
        //   reader:
        //     Объект, осуществляющий считывание из файла.
        //
        // Возврат:
        //     Объект, считанный из файла.
        //
        // Исключения:
        //     Метод может генерировать исключения.
        //
        public abstract T ReadItem(BinaryReader reader);
        //
        // Сводка:
        //     Реализует считывание объекта T из текстового файл.
        // 
        // Параметры:
        //   reader:
        //     Объект, осуществляющий считывание из файла.
        //
        // Возврат:
        //     Объект, считанный из файла.
        //
        // Исключения:
        //     Метод может генерировать исключения.
        //
        public abstract T ReadItem(StreamReader reader);

        //
        // Сводка:
        //     Реализует считывание объекта List<T> из текстового файла.
        //
        // Параметры:
        //   path:
        //     Путь к текстовому файлу открытому для чтения.
        //
        // Возврат:
        //     Объект System.Collections.Generic.List<T>, считанный из файла.
        //
        // Исключения:
        //   [new] T:System.ArgumentNullException:
        //     Параметр path имеет значение null:
        //     - Source:
        //       Код ошибки: "-1".
        //
        //   [new] T:System.IO.InvalidDataException:
        //     Исключение возникает в случае, если первая строка файла не содержит число - количество элементов в коллекции.
        //     - Source:
        //       "0" - номер строки, в которой возникло исключение;
        //
        //   [chained] T:System.InvalidCastException:
        //     Возникает в случае возникновения исключения в методе IItemReader<T>.ReadItem(System.IO.StreamReader).
        //     - Source:
        //       "x":[1..n] - номер блока элемента, в котором возникло исключние.
        //
        //   [chained] T:System.IO.IOException:
        //     Ошибка считывания. Поток закрыт или достигнут конец потока.
        //     - Source:
        //       "-1" - если исклюючение возникло при создании потока;
        //       "0" - если исключение возникло при считывании числа из первой строки файла;
        //
        public List<T> ReadFromTextFile(string path)
        {
            if (path == null)
                throw new ArgumentNullException("Параметр path имеет значение null") { Source = "-1" };

            string res;
            bool IsParseSuccesful;
            var Collection = new List<T>();

            T Data;
            int n;
            FileStream strm;

            try
            {
                strm = File.Open(path, FileMode.Open, FileAccess.Read);
            }
            catch (Exception ex)
            {
                throw new IOException("Невозможно открыть файл.", ex) { Source = "-1" };
            }

            using (StreamReader reader = new StreamReader(strm))
            {
                try
                {
                    res = reader.ReadLine();
                }
                catch (Exception ex)
                {
                    throw new IOException("Ошибка чтения.", ex) { Source = "0" };
                }
                IsParseSuccesful = int.TryParse(res.Trim(), out n);

                if (!IsParseSuccesful)
                    throw new InvalidDataException("Первая строка текстового файла должна содержать число элементов в коллекции.") { Source = "0" };

                for (int i = 0; i < n; i++)
                {
                    try
                    {
                        Data = ReadItem(reader); ;
                    }
                    catch (Exception e)
                    {
                        throw new InvalidCastException("Не удалось cчитать элемент.", e) { Source = (i + 1).ToString() };
                    }
                    Collection.Add(Data);
                }
            }
            return Collection;
        }
        //
        // Сводка:
        //     Реализует считывание объекта List<T> из бинарного файла.
        //
        // Параметры:
        //   path:
        //     Путь к бинарному файлу открытому для чтения.
        //
        // Возврат:
        //     Объект System.Collections.Generic.List<T>, считанный из файла.
        //
        // Исключения:
        //   [new] T:System.ArgumentNullException:
        //     Параметр path имеет значение null:
        //     - Source:
        //       Код ошибки: "-1".
        //
        //   [new] T:System.IO.IOException:
        //     Ошибка считывания. Поток закрыт или достигнут конец потока.
        //     - Source:
        //       "-1" если исключение возникло при создании потока,
        //       "0" если исключение возникло при считывании числа элементов коллекции.
        //
        //   [chained] T:System.InvalidCastException:
        //     Возникает в случае возникновения исключения в методе IItemReader<T>.ReadItem(System.IO.StreamReader);
        //     - Source:
        //       "x":[1..n] - номер итерации, на который производился вызов метода IItemReader<T>.ReadItem(System.IO.StreamReader), 
        //       вызвавший исключение.
        //
        public List<T> ReadFromBinaryFile(string path)
        {
            if (path == null)
                throw new ArgumentNullException("Параметр path имеет значение null") { Source = "-1" };

            var Collection = new List<T>();
            T Data;
            int n;

            FileStream strm;

            try
            {
                strm = File.Open(path, FileMode.Open, FileAccess.Read);
            }
            catch (Exception ex)
            {
                throw new IOException("Невозможно открыть файл.", ex) { Source = "-1" };
            }

            using (BinaryReader reader = new BinaryReader(strm))
            {
                try
                {
                    n = reader.ReadInt32();
                }
                catch (Exception ex)
                {
                    throw new IOException("Не удалось считать число элементов в коллекции (32 бит).", ex) { Source = "0" };
                }
                for (int i = 0; i < n; i++)
                {
                    try
                    {
                        Data = ReadItem(reader);
                    }
                    catch (Exception e)
                    {
                        throw new InvalidCastException("Не удалось cчитать элемент.", e) { Source = (i + 1).ToString() };
                    }
                    Collection.Add(Data);
                }
            }

            return Collection;
        }
    }
}
