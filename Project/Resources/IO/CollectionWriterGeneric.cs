﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    using System.IO;
    using System.Collections.ObjectModel;

    //
    // Сводка:
    //     Является базовым классом для всех классов, содержащих методы, осуществляющие
    //     форматированную запись объекта IList<T> в текстовый и бинарный файлы. 
    //
    public abstract class CollectionWriter<T> : IItemWriter<T>
    {
        //
        // Сводка:
        //     Реализует вывод объекта T в текстовый файл.
        // 
        // Параметры:
        //   writer:
        //     Объект, осуществляющий запись в файл.
        //
        //   ST:
        //     Объект, который нужно вывести.
        //
        // Исключения:
        //     Метод может генерировать исключения.
        //
        public abstract void WriteItem(StreamWriter writer, T ST);
        //
        // Сводка:
        //     Реализует вывод объекта T в бинарный файл.
        // 
        // Параметры:
        //   writer:
        //     Объект, осуществляющий запись в файл.
        //
        //   ST:
        //     Объект, который нужно вывести.
        //
        // Исключения:
        //     Метод может генерировать исключения.
        //
        public abstract void WriteItem(BinaryWriter writer, T ST);

        //
        // Сводка:
        //     Реализует форматированный вывод элементов объекта IList<T> в текстовый файл.
        //
        // Параметры:
        //   path:
        //     Путь к текстовому файлу открытому для записи.
        //
        //   collection:
        //     Объект, который нужно вывести.
        //
        // Исключения:
        //   [new] T:System.ArgumentNullException:
        //     Параметр path или collection имеет значение null.
        //     - Source:
        //       Код ошибки: "-1".
        //
        //   [chained] T:System.InvalidCastException:
        //     Возникает в случае возникновения исключения в методе 
        //     Project.IO.IItemWriter<T>.WriteItem(System.IO.StreamWriter, T).
        //     - Source:
        //       "x":[1..n] - номер блока элемента, в котором возникло исключние.
        //
        //   [chained] T:System.IO.IOException:
        //     Ошибка записи. Поток закрыт или достигнут конец потока.
        //     - Source:
        //       "-1" - если искллючение возникло при создании потока;
        //       "0" - если исключение возникло при записи числа элементов в коллекции в первую строку файла;
        //
        public void WriteToTextFile(string path, IList<T> collection)
        {
            if (collection == null)
                throw new ArgumentNullException("Параметр collection имеет значение null.") { Source = "-1" };
            if (path == null)
                throw new ArgumentNullException("Параметр path имеет значение null.") { Source = "-1" };

            FileStream strm;

            try
            {
                strm = File.Open(path, FileMode.Create, FileAccess.Write);
            }
            catch (Exception ex)
            {
                throw new IOException("Невозможно открыть файл.", ex) { Source = "-1" };
            }
            using (StreamWriter writer = new StreamWriter(strm))
            {
                try
                {
                    writer.WriteLine(collection.Count);
                }
                catch (Exception ex)
                {
                    throw new IOException("Не удалось вывести число элементов в коллекции.", ex) { Source = "0" };
                }

                for (int i = 0; i < collection.Count; i++)
                {
                    try
                    {
                        WriteItem(writer, collection[i]);
                    }
                    catch(Exception ex)
                    {
                        throw new InvalidCastException("Не удалось вывести элемент.", ex) { Source = (i + 1).ToString() };
                    }
                }
            }
        }
        //
        // Сводка:
        //     Реализует форматированный вывод элементов коллекции IList<T> в бинарный файл.
        //
        // Параметры:
        //   path:
        //     Путь к бинарному файлу открытому для записи.
        //
        //   collection:
        //     Объект, который нужно вывести.
        //
        // Исключения:
        //   [new] T:System.ArgumentNullException:
        //     Параметр path или collection имеет значение null.
        //     - Source:
        //       Код ошибки: "-1".
        //
        //   [chained] T:System.InvalidCastException:
        //     Возникает в случае возникновения исключения в методе 
        //     Project.IO.IItemWriter<T>.WriteItem(System.IO.BinaryWriter, T).
        //     - Source:
        //       "x":[1..n] - номер блока элемента, в котором возникло исключние.
        //
        //   [chained] T:System.IO.IOException:
        //     Ошибка записи. Поток закрыт или достигнут конец потока.
        //     - Source:
        //       "-1" - если искллючение возникло при создании потока;
        //       "0" - если исключение возникло при записи числа элементов в коллекции.
        //
        public void WriteToBinaryFile(string path, IList<T> collection)
        {
            if (collection == null)
                throw new ArgumentNullException("Параметр collection имеет значение null.") { Source = "-1" };
            if (path == null)
                throw new ArgumentNullException("Параметр path имеет значение null.") { Source = "-1" };

            FileStream strm;

            try
            {
                strm = File.Open(path, FileMode.Open, FileAccess.Write);
            }
            catch (Exception ex)
            {
                throw new IOException("Невозможно открыть файл.", ex) { Source = "-1" };
            }
            using (BinaryWriter writer = new BinaryWriter(strm))
            {
                try
                {
                    writer.Write(collection.Count);
                }
                catch (Exception ex)
                {
                    throw new IOException("Не удалось вывести число элементов в коллекции.", ex) { Source = "0" };
                }

                for (int i = 0; i < collection.Count; i++)
                {
                    try
                    {
                        WriteItem(writer, collection[i]);
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidCastException("Не удалось вывести элемент.", ex) { Source = (i+1).ToString() };
                    }
                }
            }
        }
    }
}
