﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    using System.IO;

    //
    // Сводка:
    //     Позволяет объекту управлять считыванием объекта T.
    //
    interface IItemReader<T>
    {
        //
        // Сводка:
        //     Производит чтение объекта T из текстового файла.
        //
        // Параметры:
        //   reader:
        //     Объект, осуществляющий чтение из файла.
        //
        // Возврат:
        //     Объект, который нужно считать.
        //
        T ReadItem(BinaryReader reader);
        //
        // Сводка:
        //     Производит чтение объекта T из бинарного файла.
        //
        // Параметры:
        //   reader:
        //     Объект, осуществляющий чтение из файла.
        //
        // Возврат:
        //     Объект, который нужно считать.
        //
        T ReadItem(StreamReader reader);
    }
}
