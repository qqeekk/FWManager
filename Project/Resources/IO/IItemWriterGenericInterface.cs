﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    using System.IO;

    //
    // Сводка:
    //     Позволяет объекту управлять выводом объекта T.
    //
    interface IItemWriter<T>
    {
        //
        // Сводка:
        //     Производит запись объекта T в текстовый файл.
        //
        // Параметры:
        //   writer:
        //     Объект, осуществляющий запись в файл.
        //
        //   ST:
        //     Объект, который нужно вывести.
        //
        void WriteItem(StreamWriter writer, T ST);
        //
        // Сводка:
        //     Производит запись объекта T в бинарный файл.
        //
        // Параметры:
        //   writer:
        //     Объект, осуществляющий запись в файл.
        //
        //   ST:
        //     Объект, который нужно вывести.
        //
        void WriteItem(BinaryWriter writer, T ST);
    }
}
