﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    using System.IO;

    //
    // Сводка:
    //     Описывает класс, содержащий методы, реализующих считывание
    //     объекта List<Link> из текстового и бинарного файла.
    //
    class LinksCollectionReader : CollectionReader<Link>
    {
        //
        // Сводка:
        //     Реализует чтение объекта Project.Link из текстового файла.
        // 
        // Параметры:
        //   reader:
        //     Объект, осуществляющий чтение из файла.
        //
        // Возврат:
        //     Объект, считанный из файла.
        //
        // Исключения:
        //   [new] T:System.ArgumentNullException:
        //     Параметр reader имеет значение null.
        //     - Source:
        //       Код ошибки: "-1".
        //
        //   [new] T:System.IO.InvalidDataException:
        //     Не соблюдается необходимое форматирование. Например, значение числового поля представляется строкой, 
        //     содержащей символы, отличные от числовых. Исключение также может возникнуть, если допущена пустая строка.
        //     - Source:
        //       "x":[0..5] - номер строки, на которой возникло исключение.        
        //
        //   [chained] T:System.IO.IOException:
        //     Ошибка считывания. Поток закрыт или достигнут конец потока.
        //     - Source:
        //       "x":[0..5] - номер строки, на которой возникло исключение.
        //
        //   [chained] T:System.InvalidCastException:
        //     Возникает в случае возникновения исключения в конструкторе Project.А() или Project.B().
        //     - Source:
        //       "x":[1..5] - номер свойства объектов Project.Link.First и Project.Link.Second, в set-методе для которого возникло исключение.
        //
        public override Link ReadItem(StreamReader reader)
        {
            if (reader == null)
                throw new ArgumentNullException("Параметр reader имеет значение null.") { Source = "-1" };

            string res;
            int temp;
            bool IsParseSuccesful;

            A AST;
            string nameWorker;
            int years;
            char gender;

            B BST;
            string nameFactory;
            string adress;

            #region Gap Reading
            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считываения.", ex) { Source = "0" };
            }
            if (!String.IsNullOrWhiteSpace(res))
                throw new InvalidDataException("Блоки элементов должны разделяться пустыми строками.") { Source = "0" };
            #endregion

            #region Worker Reading

            //NAME
            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считывания.", ex) { Source = "1" };
            }
            if (String.IsNullOrWhiteSpace(res))
                throw new InvalidDataException("Пустая строка.") { Source = "1" };
            nameWorker = res.Trim();

            //YEARS
            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считывания.", ex) { Source = "2" };
            }
            IsParseSuccesful = int.TryParse(res.Trim(), out temp);
            if (!IsParseSuccesful)
                throw new InvalidDataException("Недопустимые символы в строке или пустая строка.") { Source = "2" };
            else years = temp;

            //GENDER
            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считывания.", ex) { Source = "3" };
            }
            res.Trim();
            if (String.IsNullOrWhiteSpace(res))
                throw new InvalidDataException("Пустая строка.") { Source = "3" };
            if (res.Length == 1)
                gender = res[0];
            else throw new InvalidDataException("Недопустимые символы в строке.") { Source = "3" };
            
            #endregion

            #region Factory Reading

            //NAME
            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считывания.", ex) { Source = "4" };
            }

            if (String.IsNullOrWhiteSpace(res))
                throw new InvalidDataException("Пустая строка.") { Source = "4" };

            nameFactory = res.Trim();

            //ADRESS
            try
            {
                res = reader.ReadLine();
            }
            catch (Exception ex)
            {
                throw new IOException("Ошибка считывания.", ex) { Source = "5" };
            }
            if (String.IsNullOrWhiteSpace(res))
                throw new InvalidDataException("Пустая строка.") { Source = "5" };

            adress = res.Trim();

            #endregion

            try
            {
                AST = new A(nameWorker, years, gender);
            }
            catch (Exception e)
            {
                throw new InvalidCastException("Ошибка при создании записи: Недопустимые значения полей", e) { Source = e.Source };
            }
            try
            {
                BST = new B(nameFactory, adress);
            }
            catch (Exception e)
            {
                var ex = new InvalidCastException("Ошибка при создании записи: Недопустимые значения полей", e);

                if (e.Source == "1") ex.Source = "4";
                if (e.Source == "2") ex.Source = "5";

                throw ex;
            }

            return new Link(AST, BST);
        }
        //
        // Сводка:
        //     Реализует чтение объекта Project.Link из бинарного файла.
        //
        // Параметры:
        //   reader:
        //     Объект, осуществляющий чтение из файла.
        //
        // Возврат:
        //     Объект, считанный из файла.
        //
        // Исключения:
        //   [new] T:System.ArgumentNullException:
        //     Параметр reader имеет значение null.
        //     - Source:
        //       Код ошибки: "-1".
        //
        //   [chained] T:System.IO.IOException:
        //     Ошибка считывания. Поток закрыт или достигнут конец потока.
        //     - Source:
        //       "x":[1..5] - номер итерации, на которой возникло исключение.
        //
        //   [chained] T:System.InvalidCastException:
        //     Возникает в случае возникновения исключения в конструкторе Project.А() или Project.B().
        //     - Source:
        //       "x":[1..5] - номер свойства объектов Project.Link.First и Project.Link.Second, в set-методе для которого возникло исключение.
        //
        public override Link ReadItem(BinaryReader reader)
        {
            if (reader == null)
                throw new ArgumentNullException("Параметр reader имеет значение null.") { Source = "-1" };

            string nameWorkers;
            int years;
            char gender;
            A AST;

            string nameFactories;
            string adress;
            B BST;

            #region Worker Reading
            try
            {
                nameWorkers = reader.ReadString();
            }
            catch (Exception ex)
            {
                throw new IOException("Не удалось считать строку (убедитесь, что кодировка символов совпадает с utf-8).", ex) { Source = "1" };
            }

            try
            {
                years = reader.ReadInt32();
            }
            catch (Exception ex)
            {
                throw new IOException("Не удалось считать число (32 бит).", ex) { Source = "2" };
            }

            try
            {
                gender = reader.ReadChar();
            }
            catch (Exception ex)
            {
                throw new IOException("Не удалось считать символ  (убедитесь, что кодировка символов совпадает с utf-8).", ex) { Source = "3" };
            }
            #endregion
            #region Factory Reading
            try
            {
                nameFactories = reader.ReadString();
            }
            catch (Exception ex)
            {
                throw new IOException("Не удалось считать строку (убедитесь, что кодировка символов совпадает с utf-8).", ex) { Source = "4" };
            }

            try
            {
                adress = reader.ReadString();
            }
            catch (Exception ex)
            {
                throw new IOException("Не удалось считать строку (убедитесь, что кодировка символов совпадает с utf-8).", ex) { Source = "5" };
            }
            #endregion

            try
            {
                AST = new A(nameWorkers, years, gender);
            }
            catch (Exception e)
            {
                throw new InvalidCastException("Ошибка при создании записи: Недопустимые значения полей", e) { Source = e.Source };
            }

            try
            {
                BST = new B(nameFactories, adress);
            }
            catch (Exception e)
            {
                var ex = new InvalidCastException("Ошибка при создании записи: Недопустимые значения полей", e);
                if (e.Source == "1") ex.Source = "4";
                if (e.Source == "2") ex.Source = "5";
                throw ex;
            }

            return new Link(AST, BST);
        }
    }
}
