﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    //
    // Сводка:
    //     Описывает класс, содержащий методы, реализующих форматированную запись 
    //     объекта IList<Link> в текстовый и бинарный файлы.
    //
    class LinksCollectionWriter : CollectionWriter<Link>
    {
        //
        // Сводка:
        //     Реализует вывод объекта Project.Link в текстовый файл.
        // 
        // Параметры:
        //   writer:
        //     Объект, осуществляющий запись в файл.
        //
        //   ST:
        //     Объект, который нужно вывести.
        //
        // Исключения:
        //   [new] T:System.ArgumentNullException: 
        //     Параметр writer или ST имеет значение null или параметр ST имеет незаполненные необязательные поля.
        //     - Source:
        //       Код ошибки: "-1".
        //
        //   [chained] T:System.IO.IOException:
        //     Ошибка записи. Поток закрыт или достигнут конец потока.
        //     - Source:
        //       "x":[0..5] - номер строки, при выводе которой возникло исключние.
        //
        public override void WriteItem(StreamWriter writer, Link ST)
        {
            if (writer == null)
                throw new ArgumentNullException("Параметр writer имеет значение null.") { Source = "-1" };
            if (ST == null)
                throw new ArgumentNullException("Параметр ST имеет значение null.") { Source = "-1" };
            if (ST.First == null || ST.Second == null)
                throw new ArgumentNullException("Параметр ST имеет незаполненные обязательные поля.") { Source = "-1" };

            int i = 0;

            try
            {
                writer.WriteLine();
                i++;
                writer.WriteLine(ST.First.Name);
                i++;
                writer.WriteLine(ST.First.Year);
                i++;
                writer.WriteLine(ST.First.Gender);
                i++;
                writer.WriteLine(ST.Second.Name);
                i++;
                writer.WriteLine(ST.Second.Adress);
            }
            catch (Exception ex)
            {
                throw new IOException("Не удалось вывести строку.", ex) { Source = i.ToString() };
            }
        }
        //
        // Сводка:
        //     Реализует вывод объекта Project.Link в бинарный файл.
        // 
        // Параметры:
        //   writer:
        //     Объект, осуществляющий запись в файл.
        //
        //   ST:
        //     Объект, который нужно вывести.
        //
        // Исключения:
        //   [new] T:System.ArgumentNullException:
        //     Параметр writer или ST имеет значение null или параметр ST имеет незаполненные необязательные поля.
        //     - Source:
        //       Код ошибки: "-1".
        //
        //   [chained] T:System.IO.IOException:
        //     Ошибка записи. Поток закрыт или достигнут конец потока.
        //     - Source:
        //       "x":[1..5] - номер свойства объекта ST, при выводе которого возникло исключние.
        //
        public override void WriteItem(BinaryWriter writer, Link ST)
        {
            if (writer == null)
                throw new ArgumentNullException("Параметр writer имеет значение null.") { Source = "-1" };
            if (ST == null)
                throw new ArgumentNullException("Параметр ST имеет значение null.") { Source = "-1" };
            if (ST.First == null || ST.Second == null)
                throw new ArgumentNullException("Параметр ST имеет незаполненные обязательные поля.") { Source = "-1" };

            int i = 1;

            try
            {
                writer.Write(ST.First.Name);
                i++;
                writer.Write(ST.First.Year);
                i++;
                writer.Write(ST.First.Gender);
                i++;
                writer.Write(ST.Second.Name);
                i++;
                writer.Write(ST.Second.Adress);
            }
            catch (Exception ex)
            {
                throw new IOException("Не удалось вывести строку.", ex) { Source = i.ToString() };
            }
        }
    }
}
