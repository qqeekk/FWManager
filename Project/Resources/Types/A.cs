﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public class A
    {
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            //
            // Исключения:
            //   System.ArgumentException:
            //     Исключение возникает в случае, если значение аргумента не является допустимым именем сотрудника.
            //     - Source:
            //       Кодовый номер поля: "1".
            //
            private set
            {
                if (value == null || value == "")
                    throw new ArgumentException("Недопустимое имя.") { Source = "1" };
                else _name = value;
            }
        }

        private int _year;
        public int Year
        {
            get
            {
                return _year;
            }
            //
            // Исключения:
            //   System.ArgumentException:
            //     Исключение возникает в случае, если значение аргумента не является допустимым годом рождения сотрудника.
            //     - Source:
            //       Кодовый номер поля: "1".
            //
            private set
            {
                if (value >= DateTime.Today.Year - 77 && value <= DateTime.Today.Year - 18)
                {
                    _year = value;
                }
                else throw new ArgumentException("Недопустимый год рождения.") { Source = "2" };
            }
        }

        private char _gender;
        public char Gender
        {
            get
            {
                return _gender;
            }
            //
            // Исключения:
            //   System.ArgumentException:
            //     Исключение возникает в случае, если значение аргумента не обозначает пол сотрудника.
            //     - Source:
            //       Кодовый номер поля: "3".
            //
            private set
            {
                if (value == 'm' || value == 'f')
                    this._gender = value;
                else throw new ArgumentException("Неверно указан пол. 'm' - муж., 'f' - жен.") { Source = "3" }; ;
            }
        }

        public A(string name, int year, char gender)
        {
            this.Name = name;
            this.Year = year;
            this.Gender = gender;
        }
    }
}
