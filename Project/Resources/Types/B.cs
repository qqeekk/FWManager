﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public class B
    {
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            //
            // Исключения:
            //   System.ArgumentException:
            //     Исключение возникает в случае, если значение аргумента не является допустимым наименованием предприятия.
            //     - Source:
            //       Кодовый номер поля: "1".
            //
            private set
            {
                if (value == null || value == "") throw new ArgumentException("Недопустимое имя предприятия.");
                else _name = value;
            }
        }

        private string _adress;
        public string Adress
        {
            get
            {
                return _adress;
            }
            //
            // Исключения:
            //   System.ArgumentException:
            //     Исключение возникает в случае, если значение аргумента не является допустимым адресом предприятия.
            //     - Source:
            //       Кодовый номер поля: "2".
            //
            private set
            {
                if (value == null || value == "") throw new ArgumentException("Недопустимый адрес предприятия.");
                else _adress = value;
            }
        }

        public B(string name, string adress)
        {
            Name = name;
            Adress = adress;
        }
    }
}
