﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public class Link
    {
        private A _first;
        public A First
        {
            get
            {
                return _first;
            }
            private set
            {
                if (value == null) throw new ArgumentOutOfRangeException();
                else _first = value;
            }
        }

        private B _second;
        public B Second
        {
            get
            {
                return _second;
            }
            private set
            {
                if (value == null) throw new ArgumentOutOfRangeException();
                else _second = value;
            }
        }

        public Link(A first, B second)
        {
            First = first;
            Second = second;
        }
    }
}
