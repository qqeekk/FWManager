﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    using System.Windows.Data;

    //
    // Сводка:
    //     Представляет класс, содержащий методы, заменяющие отображение пола сотрудника, 
    //     хранящиеся в виде символа на более полное строковое представление
    //
    [ValueConversion(typeof(char), typeof(string))]
    internal class GenderViewConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (char)value == 'm' ? "мужской" : "женский";
        }
        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value == "мужской" ? 'm' : 'f';
        }
    }
}
