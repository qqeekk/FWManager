﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    using System.Windows.Data;

    //
    // Сводка:
    //     Представляет класс, содержащий методы, устанавливающие максимальную ширину колонки в разметке, 
    //     в зависимости от размеров родительского окна.
    //
    [ValueConversion(typeof(double), typeof(double))]
    public class WidthConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((double)value - 5) / 2;
        }
        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (double)value * 2 + 5;
        }
    }
}
