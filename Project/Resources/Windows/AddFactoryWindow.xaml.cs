﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project
{
    /// <summary>
    /// Логика взаимодействия для AddFactoryWindow.xaml
    /// </summary>
    public partial class AddFactoryWindow : Window
    {
        //
        // Сводка:
        //     Главное окно, для которого был создан этот экземпляр.
        //
        private MainWindow _owner;
        private B _factory = null;

        //
        // Сводка:
        //     Возвращает экземпляр Project.B, созданный в окне.
        //
        public B Factory { get { return _factory; } }

        //
        // Сводка:
        //     Инициализирует новый экземпляр класса Project.AddFactoryWindow.
        //
        // Параметры:
        //   win:
        //     Экземпляр класса, из методов которого был вызван конструктор (Главное окно).
        //
        public AddFactoryWindow(MainWindow win)
        {
            InitializeComponent();
            _owner = win;
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "добавить элемент".
        //
        private void btnAddItem_Click(object sender, RoutedEventArgs e)
        {
            bool err = false;

            if (String.IsNullOrWhiteSpace(txtName.Text))
            {
                tbkNameError.Visibility = Visibility.Visible;
                txtName.BorderBrush = Brushes.Red;
                err = true;
            }
            else
            {
                tbkNameError.Visibility = Visibility.Hidden;
                txtName.BorderBrush = new SolidColorBrush((Color) App.Current.Resources["DefaultBorderColor"]);
            }

            if (String.IsNullOrWhiteSpace(txtAdress.Text))
            {
                tbkAdressError.Visibility = Visibility.Visible;
                txtAdress.BorderBrush = Brushes.Red;
                err = true;
            }
            else
            {
                tbkAdressError.Visibility = Visibility.Hidden;
                txtAdress.BorderBrush = new SolidColorBrush((Color) App.Current.Resources["DefaultBorderColor"]);
            }

            if(!err)
            {
                _factory = new B(txtName.Text, txtAdress.Text);
                this.Close();
            }
        }
    }
}
