﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project
{
    /// <summary>
    /// Логика взаимодействия для AddWorkerWindow.xaml
    /// </summary>
    public partial class AddWorkerWindow : Window
    {
        //
        // Сводка:
        //     Главное окно, для которого был создан этот экземпляр.
        //
        private MainWindow _owner;
        private A _worker = null;

        //
        // Сводка:
        //     Возвращает экземпляр Project.A, созданный в окне.
        //
        public A Worker { get { return _worker; } }
        //
        // Сводка:
        //     Инициализирует новый экземпляр класса Project.AddWorkerWindow.
        //
        // Параметры:
        //   win:
        //     Экземпляр класса, из методов которого был вызван конструктор (Главное окно).
        //
        public AddWorkerWindow(MainWindow win)
        {
            InitializeComponent();
            _owner = win;

            cmbAddYear.ItemsSource = win.Years;
            cmbAddYear.SelectedIndex = 49;
        }

        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "добавить элемент".
        //
        private void btnAddItem_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtAddName.Text))
            {
                tbkError.Visibility = Visibility.Visible;
                txtAddName.BorderBrush = Brushes.Red;
            }
            else
            {
                _worker = new A(txtAddName.Text, (int)cmbAddYear.SelectedItem, rdoMale.IsChecked == true ? 'm' : 'f');
                this.Close();
            }
        }
    }
}
