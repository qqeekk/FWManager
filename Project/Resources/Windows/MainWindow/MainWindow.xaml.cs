﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project
{         
    public partial class MainWindow : Window
    {
        public static string textA = @"..\textA.txt";
        public static string binA = @"..\binA.dat";
        public static string textB = @"..\textB.txt";
        public static string binB = @"..\binB.dat";
        public static string textLink = @"..\textLink.txt";
        public static string binLink = @"..\binLink.dat";
        
        //
        // Сводка:
        //     Получает список допустимых годов рождения (от 1940 до 1998). 
        //     Этот ресурс связан с некоторыми элементами управления.
        //
        public int[] Years {get;}
        
        //
        // Cводка:
        //   Градиентный красно-белый окрас активной кнопки для связанных элементов, когда связанные элементы отсутствуют.
        //
        private LinearGradientBrush _redBrush = new LinearGradientBrush(Colors.Red, Colors.White, new Point(0, 0), new Point(1, 1));
        //
        // Cводка:
        //   Градиентный зелено-белый окрас активной кнопки для связанных элементов, когда связанные элементы существуют.
        //
        private LinearGradientBrush _greenBrush = new LinearGradientBrush(Colors.Green, Colors.White, new Point(0, 0), new Point(1, 1));
        //
        // Cводка:
        //   Градиентный зелено-белый окрас неактивной кнопки для связанных элементов.
        //
        private LinearGradientBrush _blueBrush = new LinearGradientBrush(Colors.Blue, Colors.White, new Point(0, 0), new Point(1, 1));
        //
        // Сводка:
        //     Составляет полное сообщение об ошибке на основе сообщений вложенных исключений.
        //
        // Параметры:
        //   ex:
        //     Исключение.
        //
        // Возврат:
        //     Строка, содержащая сообщение.
        //
        private string ExMessageBuilder(Exception ex)
        {
            if (ex == null) return "";
            return String.Format("∟ {0}\n{1}", ex.Message, ExMessageBuilder(ex.InnerException));
        }
        //
        // Сводка:
        //     Все исключения (ex), возникшие внутри метода ACollectionReader.ReadFromTextFile -или- 
        //     BCollectionReader.ReadFromTextFile -или- ACollectionReader.ReadFromBinaryFile -или-
        //     BCollectionReader.ReadFromBinaryFile -или- ACollectionWriter.WriteToTextFile -или- 
        //     BCollectionWriter.WriteToTextFile -или- ACollectionWriter.WriteToBinaryFile -или-
        //     BCollectionWriter.WriteToBinaryFile -или- LinksCollectionReader.ReadFromTextFile -или-
        //     LinksCollectionReader.ReadFromBinaryFile -или- LinksCollectionWriter.WriteToTextFile -или-
        //     LinksCollectionWriter.WriteToBinaryFile имеют один из следующих видов:
        //             ex.Source == "-1", если исключение возникло до начала считывания/записи информации с файла,
        //       -или- ex.Source == "0", если исключение возникло при считывании/записи первого элемента с файла,
        //       -или- если исключение возникло при считывании/записи одного из блоков информации об элементе коллекции:
        //                 ex.Source == "x":[1..n], где x - номер блока элемента, на котором возникло исключение,
        //                 n-количество блоков,
        //             -и- ex.InnerException - ссылка на непустой экземпляр Project.Readers.FailedToReadException,
        //             -и- ex.InnerException.Source == "x":[1..blockHeight], где x - номер подстроки(поля) в блоке элемента,
        //                 на которой возникло исключение, blockHeight - количество подстрок(полей) в блоке элемента -или-
        //                 ex.InnerException.Source == "0", если исключение вознико при выводе строки, предшевствующей блоку
        //                 элемента -или- ex.InnerException.Source = "-1", если исключение возникло до начала считывания/записи
        //                 блока элемента.
        //
        //     Этой информации достаточно, чтобы cгенерировать подробное сообщение о возникшем исключении с указанием
        //     на "координаты" поля, неверно заполненного пользователем и программой, которое приводит к ошибке считывания.
        //     Данный метод призван создать такое сообщение.
        //
        // Параметры:
        //   ex:
        //     Исключение.
        //
        // Возврат:
        //     Строка, содержащая сообщение с "координатами".
        //
        // Исключения:
        //   System.ArgumentException:
        //     Переданное в качестве аргумента исключение возникло вне метода ACollectionReader.ReadFromTextFile -или- 
        //     BCollectionReader.ReadFromTextFile -или- ACollectionReader.ReadFromBinaryFile -или-
        //     BCollectionReader.ReadFromBinaryFile -или- ACollectionWriter.WriteToTextFile -или- 
        //     BCollectionWriter.WriteToTextFile -или- ACollectionWriter.WriteToBinaryFile -или-
        //     BCollectionWriter.WriteToBinaryFile -или- LinksCollectionReader.ReadFromTextFile -или-
        //     LinksCollectionReader.ReadFromBinaryFile -или- LinksCollectionWriter.WriteToTextFile -или-
        //     LinksCollectionWriter.WriteToBinaryFile.
        private string ExSourceBuilder(Exception ex)
        {
            string s = ex.Source;
            string txtFormat = "Ошибка в строке: {0}. (Блок: {1}/Поле: {2})";
            string binFormat = "Ошибка в блоке элемента: {0}. (Поле: {1})";

            if (s == "-1") return "";

            if (ex.TargetSite == (MethodBase)typeof(CollectionReader<>).GetMethod("ReadFromTextFile")
                || ex.TargetSite == (MethodBase)typeof(CollectionWriter<>).GetMethod("WriteToTextFile"))
            {
                if (s == "0") return "Ошибка в строке: 1.";

                int blockHeight;

                if (ex.InnerException?.TargetSite == (MethodBase)typeof(Project.ACollectionReader).GetMethod("ReadItem", new Type[] { typeof(StreamReader) })
                      || ex.InnerException?.TargetSite == (MethodBase)typeof(Project.ACollectionWriter).GetMethod("WriteItem", new Type[] { typeof(StreamWriter), typeof(A) }))
                    blockHeight = 4;
                else if (ex.InnerException?.TargetSite == (MethodBase)typeof(Project.BCollectionReader).GetMethod("ReadItem", new Type[] { typeof(StreamReader) })
                      || ex.InnerException?.TargetSite == (MethodBase)typeof(Project.BCollectionWriter).GetMethod("WriteItem", new Type[] { typeof(StreamWriter), typeof(B) }))
                    blockHeight = 3;
                else if (ex.InnerException?.TargetSite == (MethodBase)typeof(Project.LinksCollectionReader).GetMethod("ReadItem", new Type[] { typeof(StreamReader) })
                      || ex.InnerException?.TargetSite == (MethodBase)typeof(Project.LinksCollectionWriter).GetMethod("WriteItem", new Type[] { typeof(StreamWriter), typeof(Link) }))
                    blockHeight = 6;
                else throw new ArgumentException("Переданное исключение было вызвано вне методов допустимых объектов.");

                int blockNumber = int.Parse(s);
                int fieldNumber = int.Parse(ex.InnerException.Source);

                if (fieldNumber == -1) return "";
                return String.Format(txtFormat, 2 + (blockNumber - 1) * blockHeight + fieldNumber, blockNumber, fieldNumber);
            }
            else if (ex.TargetSite == (MethodBase)typeof(CollectionReader<>).GetMethod("ReadFromBinaryFile")
                    || ex.TargetSite == (MethodBase)typeof(CollectionWriter<>).GetMethod("WriteToBinaryFile"))
            {
                if (s == "0") return "";

                if (ex.InnerException?.TargetSite != (MethodBase)typeof(Project.ACollectionReader).GetMethod("ReadItem", new Type[] { typeof(BinaryReader) })
                    && ex.InnerException?.TargetSite != (MethodBase)typeof(Project.ACollectionWriter).GetMethod("WriteItem", new Type[] { typeof(BinaryWriter), typeof(A) })
                    && ex.InnerException?.TargetSite != (MethodBase)typeof(Project.BCollectionReader).GetMethod("ReadItem", new Type[] { typeof(BinaryReader) })
                    && ex.InnerException?.TargetSite != (MethodBase)typeof(Project.BCollectionWriter).GetMethod("WriteItem", new Type[] { typeof(BinaryWriter), typeof(B) })
                    && ex.InnerException?.TargetSite == (MethodBase)typeof(Project.LinksCollectionReader).GetMethod("ReadItem", new Type[] { typeof(StreamReader) })
                    && ex.InnerException?.TargetSite == (MethodBase)typeof(Project.LinksCollectionWriter).GetMethod("WriteItem", new Type[] { typeof(StreamWriter), typeof(Link) }))
                    throw new ArgumentException("Переданное исключение было вызвано вне методов допустимых объектов.");

                int blockNumber = int.Parse(s);
                int fieldNumber = int.Parse(ex.InnerException.Source);

                if (fieldNumber == -1) return "";
                return String.Format(binFormat, blockNumber, fieldNumber);
            }
            else throw new ArgumentException("Переданное исключение было вызвано вне методов допустимых объектов.");
        }

        //
        // Сводка:
        //     Возвращает визуальный объект заданного типа среди потомков заданного
        //     родительского объекта.
        //
        // Параметры: 
        //   parent:
        //     Визуальный родительский элемент.
        //
        // Возврат:
        //     Элемент-потомок указанного типа. Или первый найденный, если таких элементов несколько.
        //     Или null, если элементы указанного типа отсутствуют среди потомков.
        private T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);

            //Получаем количество прямых потомков текущего элемента.
            int n = VisualTreeHelper.GetChildrenCount(parent);

            //перебираем всех его прямых потомков.
            for (int i = 0; i < n; i++)
            {
                //получаем прямого потомка текущего элемента по индексу.
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);

                //пытаемся привести его к указанному типу.
                child = v as T;

                //если приведение невозможно
                if (child == null)
                {
                    //ищем элемент среди потомков данного потомка.
                    child = GetVisualChild<T>(v);
                }
                //иначе - элемент найден. Прерываем цикл и возвращаем элемент.
                if (child != null)
                    break;
            }

            return child;
        }
        //
        // Сводка:
        //     Вызывается другими методами этого класса в случае частичного или полного изменения одной
        //     из коллекций сотрудников или предприятий. Отвечает за видимость кнопок добавления связей.
        //
        private void CheckForLinkButtons()
        {
            if (FactoriesList?.Count != 0 && WorkersList?.Count != 0)
            {
                btnAddLinkWithWorkers.IsEnabled = true;
                btnAddLinkWithFactories.IsEnabled = true;
            }
            else
            {
                btnAddLinkWithWorkers.IsEnabled = false;
                btnAddLinkWithFactories.IsEnabled = false;
            }
        }

        //
        // Сводка:
        //     Инициализирует экземпляр класса Project.MainWindow
        //
        public MainWindow()
        {
            InitializeComponent();

            //стандартные директории.
            btnLoadWorkers.Tag = textA; 
            btnLoadWorkers.ToolTip = (string) textA;

            btnSaveWorkers.Tag = textA;
            btnSaveWorkers.ToolTip = (string)textA;

            btnSaveFactories.Tag = textB;
            btnSaveFactories.ToolTip = (string)textB;

            btnLoadFactories.Tag = textB;
            btnLoadFactories.ToolTip = (string)textB;

            btnSaveLinks.Tag = textLink;
            btnSaveLinks.ToolTip = textLink;

            btnLoadLinks.Tag = textLink;
            btnLoadLinks.ToolTip = textLink;

            dgdWorkers.ItemsSource = WorkersList;
            dgdFactories.ItemsSource = FactoriesList;

            //заполнение массива возрастов.
            Years = new int[59];
            for (int i = 0; i < 59; i++)
                Years[i] = DateTime.Now.Year - 77 + i;

            //установка выбранных возрастов по умолчанию.
            cmbYearFromSearch.SelectedIndex = 0;
            cmbYearToSearch.SelectedIndex = 58;
        }

        //
        // Сводка:
        //     Открывает файловый диалог. Получает наименование файла для считывания/записи.
        //
        // Возврат:
        //     Строка, содержащая наименования файла. null в случае,
        //     если пользователь закрыл файловый диалог.
        private string GetFile()
        {
            string filename = null;
            var ofg = new Microsoft.Win32.OpenFileDialog();

            ofg.Filter = "Data Files (*.txt;*.dat;*.bin)|*.txt;*.dat;*.bin";

            bool? result = ofg.ShowDialog();
            if (result == true)
            {
                filename = ofg.FileName;
            }

            return filename;
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "сменить директорию".
        //
        private void btnChangeDir_Click(object sender, RoutedEventArgs e)
        {
            Button button;
            try
            {
                button = (Button)sender;
            }
            catch
            {
                throw new ArgumentException();
            }
            string filename = GetFile();
            if (filename == null) return;

            if (button == btnChangeLoadFactoriesDir)
            {
                btnLoadFactories.Tag = filename;
                btnLoadFactories.ToolTip = filename;
            }
            else if (button == btnChangeSaveFactoriesDir)
            {
                btnSaveFactories.Tag = filename;
                btnSaveFactories.ToolTip = filename;
            }
            else if (button == btnChangeLoadWorkersDir)
            {
                btnLoadWorkers.Tag = filename;
                btnLoadWorkers.ToolTip = filename;
            }
            else if (button == btnChangeSaveWorkersDir)
            {
                btnSaveWorkers.Tag = filename;
                btnSaveWorkers.ToolTip = filename;
            }
            else if (button == btnChangeLoadLinksDir)
            {
                btnLoadLinks.Tag = filename;
                btnLoadLinks.ToolTip = filename;
            }
            else if (button == btnChangeSaveLinksDir)
            {
                btnSaveLinks.Tag = filename;
                btnSaveLinks.ToolTip = filename;
            }
            else
                throw new ArgumentException();
        }

    }
}