﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;


namespace Project
{
    //Factories part
    public partial class MainWindow : Window
    {
        //
        // Cводка:
        //   Коллекция предприятий для отображения.
        //
        public ObservableCollection<B> FactoriesList = new ObservableCollection<B>();

        //
        // Сводка:
        //     Вызывается другими методами этого класса в случае для блокирования/разблокирования
        //     элементов управления в интерфейсе.
        //
        // Параметры:
        //   choice:
        //     true - если требуется заблокировать интерфейс, false - если разблокировать. 
        //
        private void LockFactoriesInterface(bool choice)
        {
            //сокрытие кнопки "связать".
            btnBindFactoryWithWorker.Visibility = !choice ? Visibility.Hidden : Visibility.Visible;

            //блокирование вкладки сотрудников.
            tabWorkers.IsEnabled = !choice;
            tabLinks.IsEnabled = !choice;

            //деактивация кнопок
            btnAddFactory.IsEnabled = !choice;
            btnChangeLoadFactoriesDir.IsEnabled = !choice;
            btnChangeSaveFactoriesDir.IsEnabled = !choice;
            btnClearFactories.IsEnabled = !choice;
            btnLoadFactories.IsEnabled = !choice;
            btnSaveFactories.IsEnabled = !choice;

            //сокрытие колонки кнопок в таблице предприятий.
            dgdFactories.Columns[0].Visibility = !choice ? Visibility.Visible : Visibility.Hidden;

            //активация кнопки "удалить элемент" в случае, если выбрана строка.
            if (!choice && dgdFactories.SelectedCells?.Count != 0)
                btnDeleteFactory.IsEnabled = true;
            else
                btnDeleteFactory.IsEnabled = false;

            //сдвиг шторки, содержащей таблицу связанных сотрудников.
            ColumnDefinition it = grdFactories.ColumnDefinitions[0];
            it.Width = new GridLength(!choice ? 0 : it.MaxWidth);

            //сдвиг заголовка режима блокировки интерфейса.
            grdFactoriesWrap.RowDefinitions[0].Height = !choice ? new GridLength(0) : GridLength.Auto;
        }

        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "закрыть". Выход из режима добавления связи.
        //
        private void btnCloseFactories_Click(object sender, RoutedEventArgs e)
        {
            //заблокируем элементы управления интерфейса.
            LockFactoriesInterface(false);

            //обнулим список (не)связанных сотрудников.
            dgdRelatedWorkers.ItemsSource = null;

            //восстановим режим отображения таблицы.
            if (dgdRelatedWorkers.Visibility == Visibility.Hidden)
            {
                //+непустая таблица, -надпись
                dgdRelatedWorkers.Visibility = Visibility.Visible;
                lblNothingFactories.Visibility = Visibility.Hidden;
            }

            //сбросим отображение списка связанных сотрудников во вкладке "сотрудники".
            if (dgdWorkers.SelectedCells?.Count != 0)
            {
                DataGridRow row = dgdWorkers.ItemContainerGenerator.ContainerFromIndex(0) as DataGridRow;
                row.IsSelected = true;
                row.IsSelected = false;
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "искать".
        //
        private void btnSearchFactory_Click(object sender, RoutedEventArgs e)
        {
            if (FactoriesList == null) return;

            //считываем значения полей из анкеты поиска предприятия.
            string name = txtFactoryNameSearch.Text.Trim().ToLower();
            string adress = txtFactoryAdressSearch.Text.Trim().ToLower();

            //сохраним выборку элементов из списка предприятия в соответствии с правилами выборки.
            IEnumerable<B> subcollection = from item in FactoriesList
                                           where (item.Name.ToLower().Contains(name) || name.Contains(item.Name.ToLower()))
                                                 && item.Adress.ToLower().Contains(name) || name.Contains(item.Adress.ToLower())
                                           select item;
            //обновим таблицу сотрудников.
            dgdFactories.ItemsSource = subcollection;
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "показать все".
        //
        private void btnShowAllFactories_Click(object sender, RoutedEventArgs e)
        {
            dgdFactories.ItemsSource = FactoriesList;
        }

        //
        // Сводка:
        //     Получает список сотрудников, связанных с данным предприятием.
        //
        // Параметры:
        //   factory:
        //     Отправной объект Project.B.
        //
        // Возврат:
        //     Список связанных сотрудников.
        //
        // Исключения:
        //   T:System.ArgumentNullException:
        //     Параметр factory имеет значение null.
        //
        private List<A> GetRelatedWorkers(B factory)
        {
            List<A> relatedWorkers = new List<A>();

            foreach (Link item in LinksList)
            {
                if (item.Second == factory)
                {
                    relatedWorkers.Add(item.First);
                }
            }
            return relatedWorkers;
        }
        //
        // Сводка:
        //     Получает список сотрудников, НЕсвязанных с данным предприятием.
        //
        // Параметры:
        //   factory:
        //     Отправной объект Project.B.
        //
        // Возврат:
        //     Список НЕсвязанных сотрудников.
        //
        // Исключения:
        //   T:System.ArgumentNullException:
        //     Параметр factory имеет значение null.
        //
        private List<A> GetUnrelatedWokers(B factory)
        {
            List<A> unrelatedWorkers = new List<A>();

            //получим список связанных сотрудников.
            List<A> relatedWorkers = GetRelatedWorkers(factory);

            //вычтем его из списка сотрудников.
            foreach (A item in WorkersList)
            {
                if (!relatedWorkers.Contains(item))
                    unrelatedWorkers.Add(item);
            }

            return unrelatedWorkers;
        }

        //
        // Сводка:
        //     Обрабатывает событие выбора строки таблицы предприятий.
        //
        private void rowFactorySelected(object sender, RoutedEventArgs e)
        {

            B factory = (B)dgdFactories.SelectedCells[0].Item;

            //проверка на условие находится ли программа в режиме добавления связи

            //нейтральный режим
            if (!btnBindFactoryWithWorker.IsVisible)
            {
                //активация кнопки удаления предприятия
                btnDeleteFactory.IsEnabled = true;

                //активация кнопки удаления связи
                if (dgdRelatedWorkers.SelectedCells?.Count != 0)
                    btnDeleteLinkWithWorkers.IsEnabled = true;
            }
            //режим добавления связи
            else
            {
                //получение списка несвязанных сотрудников
                var unrelatedWorkers = new ObservableCollection<A>( GetUnrelatedWokers(factory));
                //привязка списка к левой таблице
                dgdRelatedWorkers.ItemsSource = unrelatedWorkers;

                //выбор варианта отображения таблицы (по св-ву пустой/непустой)

                //пустой
                if (unrelatedWorkers.Count == 0)
                {
                    //-пустая таблица, +надпись
                    dgdRelatedWorkers.Visibility = Visibility.Hidden;
                    lblNothingFactories.Visibility = Visibility.Visible;
                }
                //непустой
                else if (dgdRelatedWorkers.Visibility == Visibility.Hidden)
                {
                    //+непустая таблица, -надпись
                    dgdRelatedWorkers.Visibility = Visibility.Visible;
                    lblNothingFactories.Visibility = Visibility.Hidden;
                }

                //активация кнопки "связать" в случае, если выбраны строки в левой таблице
                if (dgdRelatedWorkers.SelectedCells?.Count != 0)
                    btnBindFactoryWithWorker.IsEnabled = true;
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие выбора строки таблицы связанных сотрудников.
        //
        private void rowRelatedWorkerSelected(object sender, RoutedEventArgs e)
        {

            if (dgdFactories.SelectedCells?.Count != 0)
            {
                //активация кнопки удаления связи в обычном режиме
                if (!btnBindFactoryWithWorker.IsVisible)
                    btnDeleteLinkWithWorkers.IsEnabled = true;

                //активация кнопки "связать"
                else
                    btnBindFactoryWithWorker.IsEnabled = true;
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие отмены выбора строки таблицы предприятий.
        //
        private void rowFactoryUnselected(object sender, RoutedEventArgs e)
        {
            //деактивация кнопки "удалить элемент"
            btnDeleteFactory.IsEnabled = false;

            //деактивация кнопки "связать"
            btnBindFactoryWithWorker.IsEnabled = false;

            //деактивация кнопки "удалить связь"
            btnDeleteLinkWithWorkers.IsEnabled = false;

            //обнуление таблицы связанных сотрудников.
            dgdRelatedWorkers.ItemsSource = null;

            //применение стандартного окраса кнопки для связанных элементов.
            if (dgdFactories.Columns[0].Visibility == Visibility.Visible)
            {
                DataGridRow row = (DataGridRow)sender;
                GetVisualChild<Button>(row).Background = _blueBrush;
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие отмены выбора строки таблицы связанных сотрудников.
        //
        private void rowRelatedWorkerUnselected(object sender, RoutedEventArgs e)
        {
            //деактивация кнопки "связать"
            btnBindFactoryWithWorker.IsEnabled = false;

            //деактивация кнопки "удалить связь".
            btnDeleteLinkWithWorkers.IsEnabled = false;
        }

        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки для связанных сотрудников.
        //
        private void btnFactoryExpand_Click(object sender, RoutedEventArgs e)
        {
            B factory = (B)((Button)sender).DataContext;

            //сдвиг шторки, содержащей таблицу связанных сотрудников.
            ColumnDefinition it = grdFactories.ColumnDefinitions[0];

            if (it.ActualWidth == 0)
                it.Width = new GridLength(it.MaxWidth);

            //получим список связанных сотрудников.
            var workers = new ObservableCollection<A>( GetRelatedWorkers(factory));

            //смена окраса кнопки для связанных элементов.
            if (workers.Count != 0)
                ((Button)sender).Background = _greenBrush;
            else
                ((Button)sender).Background = _redBrush;

            dgdRelatedWorkers.ItemsSource = workers;
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "добавить элемент".
        //
        private void btnAddFactory_Click(object sender, RoutedEventArgs e)
        {
            //создадим окно добавления предприятия с указанием на текущее окно, откроем его.
            var win = new AddFactoryWindow(this);
            win.ShowDialog();

            //если сохраненное значение отлично от null, добавим его в список. 
            if (win.Factory != null)
                FactoriesList.Add(win.Factory);

            //проверим на возможность отображения кнопок добавления связи.
            CheckForLinkButtons();
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "удалить элемент".
        //
        private void btnDeleteFactory_Click(object sender, RoutedEventArgs e)
        {
            //получим элемент из выбранной строки.
            var row = dgdFactories.SelectedCells;
            B factory;

            if (row?.Count != 0)
                factory = (B)row[0].Item;
            else return;

            //удалим связи с этим элементом.
            LinksList.RemoveAll(item => item.Second == factory);

            //удалим элемент.
            FactoriesList.Remove(factory);
            dgdRelatedWorkers.ItemsSource = null;

            //сбросим отображение списка связанных предприятий во вкладке "сотрудники".
            if (dgdWorkers.SelectedCells?.Count != 0)
            {
                DataGridRow row1 = dgdWorkers.ItemContainerGenerator.ContainerFromIndex(0) as DataGridRow;
                row1.IsSelected = true;
                row1.IsSelected = false;
            }

            //проверим на возможность отображения кнопок добавления связи.
            CheckForLinkButtons();
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "сохранить".
        //
        private void btnSaveFactories_Click(object sender, RoutedEventArgs e)
        {
            string path = (string)btnSaveFactories.Tag;
            string format = path.Substring(path.LastIndexOf('.'));
            BCollectionWriter writer = new BCollectionWriter();

            //хранит метку произошла ли запись успешно.
            bool flag = false;

            //обработка пустого списка
            if (FactoriesList.Count == 0)
            {
                MessageBox.Show("Список пуст.");
                return;
            }

            //запись в текстовый файл.
            if (format == ".txt")
            {
                try
                {
                    writer.WriteToTextFile(path, FactoriesList);
                    flag = true;
                }
                catch (Exception ex)
                {
                    //вывод сообщения об ошибке.
                    MessageBox.Show(ExMessageBuilder(ex), ExSourceBuilder(ex));
                    return;
                }
            }

            //запись в бинарный файл.
            if (format == ".bin" || format == ".dat")
            {
                try
                {
                    writer.WriteToBinaryFile(path, FactoriesList);
                    flag = true;
                }
                catch (Exception ex)
                {
                    //вывод сообщения об ошибке.
                    MessageBox.Show(ExMessageBuilder(ex), ExSourceBuilder(ex));
                    return;
                }
            }

            //вывод сообщения об успешной записи.
            if (flag == true)
                MessageBox.Show(String.Format("Сохранено.\nПуть: {0}", path));
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "загрузить".
        //
        private void btnLoadFactories_Click(object sender, RoutedEventArgs e)
        {
            List<B> tempB = null;
            string path = (string)btnLoadFactories.Tag;
            string format = path.Substring(path.LastIndexOf('.'));
            BCollectionReader reader = new BCollectionReader();

            //считывание из тесктового файла.
            if (format == ".txt")
            {
                try
                {
                    tempB = reader.ReadFromTextFile(path);
                }
                catch (Exception ex)
                {
                    //считывание из тесктового файла.
                    MessageBox.Show(ExMessageBuilder(ex), ExSourceBuilder(ex));
                    return;
                }
            }

            //считывание из бинарного файла.
            if (format == ".bin" || format == ".dat")
            {
                try
                {
                    tempB = reader.ReadFromBinaryFile(path);
                }
                catch (Exception ex)
                {
                    //считывание из тесктового файла.
                    MessageBox.Show(ExMessageBuilder(ex), ExSourceBuilder(ex));
                    return;
                }
            }

            //вывод сообщения об успешном считывании.
            if (tempB?.Count != 0)
            {
                FactoriesList = new ObservableCollection<B>(tempB);
                MessageBox.Show(String.Format("Загружено.\nПуть: {0}", path));
            }

            if(dgdFactories.ItemsSource != FactoriesList)
                dgdFactories.ItemsSource = FactoriesList;

            //сброс отображение списка связанных предприятий во вкладке "сотрудники".
            if (dgdWorkers.SelectedCells?.Count != 0)
            {
                DataGridRow row = dgdWorkers.ItemContainerGenerator.ContainerFromIndex(0) as DataGridRow;
                row.IsSelected = true;
                row.IsSelected = false;
            }

            //проверка на возможность отображения кнопок добавления связи.
            CheckForLinkButtons();
        }

        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "удалить связь".
        //
        private void btnDeleteLinkWithWorkers_Click(object sender, RoutedEventArgs e)
        {
            //получим элементы из выбранных строк.
            var row1 = dgdRelatedWorkers.SelectedCells;
            var row2 = dgdFactories.SelectedCells;
            A worker = null;
            B factory = null;

            if (row1?.Count != 0)
                worker = (A)row1[0].Item;
            if (row2?.Count != 0)
                factory = (B)row2[0].Item;
            else return;

            //удалим связь (все связи).
            LinksList.RemoveAll(item => item.First == worker && item.Second == factory);

            //получим список несвязанных предприятий.
            dgdRelatedWorkers.ItemsSource = new ObservableCollection<A>( GetRelatedWorkers(factory));

            //сброс отображения списка связанных предприятий во вкладке "сотрудники".
            if (dgdWorkers.SelectedCells?.Count != 0)
            {
                DataGridRow row = dgdWorkers.ItemContainerGenerator.ContainerFromIndex(0) as DataGridRow;
                row.IsSelected = true;
                row.IsSelected = false;
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "добавить связь".
        //
        private void btnAddLinkWithWorkers_Click(object sender, RoutedEventArgs e)
        {
            if (FactoriesList?.Count != 0 && WorkersList?.Count != 0)
            {
                //разблокируем элементы управления интерфейса.
                LockFactoriesInterface(true);

                if (dgdFactories.SelectedCells?.Count != 0)
                {
                    //получим список несвязанных предприятий.
                    var unrelatedWorkers = new ObservableCollection<A>(GetUnrelatedWokers((B)dgdFactories.SelectedCells[0].Item));
                    dgdRelatedWorkers.ItemsSource = unrelatedWorkers;

                    //смена варианта отображения таблицы.
                    if (unrelatedWorkers.Count == 0)
                    {
                        //-пустая таблица, +надпись
                        dgdRelatedWorkers.Visibility = Visibility.Hidden;
                        lblNothingFactories.Visibility = Visibility.Visible;
                    }
                }
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "связать".
        //
        private void btnBindFactoryWithWorker_Click(object sender, RoutedEventArgs e)
        {
            if (dgdFactories.SelectedCells?.Count != 0 && dgdRelatedWorkers.SelectedCells?.Count != 0)
            {
                //получим элементы из выбранных строк.
                var row1 = dgdRelatedWorkers.SelectedCells;
                var row2 = dgdFactories.SelectedCells;
                A worker = null;
                B factory = null;

                if (row1?.Count != 0)
                    worker = (A)row1[0].Item;
                if (row2?.Count != 0)
                    factory = (B)row2[0].Item;
                else return;

                //добавим новую связь.
                LinksList.Add(new Link(worker, factory));

                //получим список несвязанных сотрудников снова.
                var unrelatedWorkers = new ObservableCollection<A>(GetUnrelatedWokers(factory));
                dgdRelatedWorkers.ItemsSource = unrelatedWorkers;

                //смена варианта отображения таблицы.
                if (unrelatedWorkers.Count == 0)
                {
                    dgdRelatedWorkers.Visibility = Visibility.Hidden;
                    lblNothingFactories.Visibility = Visibility.Visible;
                }
                else if (dgdRelatedWorkers.Visibility == Visibility.Hidden)
                {
                    dgdRelatedWorkers.Visibility = Visibility.Visible;
                    lblNothingFactories.Visibility = Visibility.Hidden;
                }                              
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "очистить".
        //
        private void btnClearFactories_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult ch = MessageBox.Show("Вы уверены, что хотите очистить список предприятий?\nВсе связи будут удалены.", "", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (ch == MessageBoxResult.Yes)
            {
                //очистим список придприятий.
                FactoriesList.Clear();

                //очистим список связей.
                LinksList.Clear();

                //проверка на возможность отображения кнопок добавления связи.
                CheckForLinkButtons();

                //сброс отображения списка связанных сотрудников во вкладке "сотрудники".
                if (dgdWorkers.SelectedCells?.Count != 0)
                {
                    DataGridRow row = dgdWorkers.ItemContainerGenerator.ContainerFromIndex(0) as DataGridRow;
                    row.IsSelected = true;
                    row.IsSelected = false;
                }
            }
        }
    }
}
