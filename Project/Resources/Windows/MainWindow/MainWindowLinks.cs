﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;


namespace Project
{
    partial class MainWindow : Window
    {
        //
        // Сводка:
        //     Список актуальных связей.
        //
        public List<Link> LinksList = new List<Link>();
        // 
        // Сводка:
        //     Удаляет связи над несуществующими элементами.
        // 
        // Параметры:
        //   list:
        //     Объект IList<Link> включающий несуществующие связи.
        //
        // Возврат:
        //     Объект List<Link> без несуществующих связей.
        //
        private List<Link> CheckAndCut(IList<Link> list)
        {
            if (WorkersList == null || FactoriesList == null)
                return new List<Link>();

            var result = new List<Link>();
            var workers = new List<A>(WorkersList);
            var factories = new List<B>(FactoriesList);
            IComparer<A> cmpA = Comparer<A>.Create((x, y) =>
            {
                int a = string.Compare(x.Name, y.Name);
                if (a != 0)
                    return a;
                if (x.Year != y.Year) 
                    return x.Year > y.Year ? 1 : -1;
                if (x.Gender != y.Gender)
                    return x.Gender > y.Gender ? 1 : -1;
                return 0;
            });
            IComparer<B> cmpB = Comparer<B>.Create((x, y) =>
            {
                int a = string.Compare(x.Name, y.Name);
                if (a != 0) return a;
                return string.Compare(x.Adress, y.Adress);
            });

            workers.Sort(cmpA);
            factories.Sort(cmpB);

            int w, f;

            foreach(var item in list)
            {
                w = workers.BinarySearch(item.First, cmpA);
                if (w < 0) continue;

                f = factories.BinarySearch(item.Second, cmpB);
                if (f >= 0) result.Add(new Link(workers[w], factories[f]));
            }

            return result;
        }

        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "сохранить".
        //
        private void btnSaveLinks_Click(object sender, RoutedEventArgs e)
        {
            string path = (string)btnSaveLinks.Tag;
            string format = path.Substring(path.LastIndexOf('.'));
            LinksCollectionWriter writer = new LinksCollectionWriter();

            //хранит метку произошла ли запись успешно.
            bool flag = false;

            //обработка пустого списка
            if (LinksList.Count == 0)
            {
                MessageBox.Show("Список пуст.");
                return;
            }

            //запись в текстовый файл.
            if (format == ".txt")
            {
                try
                {
                    writer.WriteToTextFile(path, LinksList);
                    flag = true;
                }
                catch (Exception ex)
                {
                    //вывод сообщения об ошибке.
                    MessageBox.Show(ExMessageBuilder(ex), ExSourceBuilder(ex));
                    return;
                }
            }

            //запись в бинарный файл.
            if (format == ".bin" || format == ".dat")
            {
                try
                {
                    writer.WriteToBinaryFile(path, LinksList);
                    flag = true;
                }
                catch (Exception ex)
                {
                    //вывод сообщения об ошибке.
                    MessageBox.Show(ExMessageBuilder(ex), ExSourceBuilder(ex));
                    return;
                }
            }

            //вывод сообщения об успешной записи.
            if (flag == true)
                MessageBox.Show(String.Format("Сохранено.\nПуть: {0}", path));
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "загрузить".
        //
        private void btnLoadLinks_Click(object sender, RoutedEventArgs e)
        {
            if(FactoriesList?.Count != 0 && WorkersList?.Count != 0)
            {
                List<Link> tempLink = null;
                string path = (string)btnLoadLinks.Tag;
                string format = path.Substring(path.LastIndexOf('.'));
                LinksCollectionReader reader = new LinksCollectionReader();

                //считывание из тесктового файла.
                if (format == ".txt")
                {
                    try
                    {
                        tempLink = reader.ReadFromTextFile(path);
                    }
                    catch (Exception ex)
                    {
                        //считывание из тесктового файла.
                        MessageBox.Show(ExMessageBuilder(ex), ExSourceBuilder(ex));
                        return;
                    }
                }

                //считывание из бинарного файла.
                if (format == ".bin" || format == ".dat")
                {
                    try
                    {
                        tempLink = reader.ReadFromBinaryFile(path);
                    }
                    catch (Exception ex)
                    {
                        //считывание из тесктового файла.
                        MessageBox.Show(ExMessageBuilder(ex), ExSourceBuilder(ex));
                        return;
                    }
                }

                //вывод сообщения об успешном считывании.
                if (tempLink?.Count != 0)
                {
                    LinksList = CheckAndCut(tempLink);
                    string message = "Загружено.\nПуть: {0}\nСвязей считано: {1}\nСвязей не обнаружено (удалено): {2}\nДобавлено: {3}";
                    MessageBox.Show(String.Format(message, path, tempLink.Count, tempLink.Count - LinksList.Count, LinksList.Count));
                }

                //сброс отображение списка связанных предприятий во вкладке "сотрудники" и "предприятия".
                if (dgdWorkers.SelectedCells?.Count != 0)
                {
                    DataGridRow row = dgdWorkers.ItemContainerGenerator.ContainerFromIndex(0) as DataGridRow;
                    row.IsSelected = true;
                    row.IsSelected = false;
                }
                if (dgdFactories.SelectedCells?.Count != 0)
                {
                    DataGridRow row = dgdFactories.ItemContainerGenerator.ContainerFromIndex(0) as DataGridRow;
                    row.IsSelected = true;
                    row.IsSelected = false;
                }
            }
            else
            {
                MessageBox.Show("Значала загрузите данные по сотрудникам и предприятиям.");
            }
        }
    }
}
