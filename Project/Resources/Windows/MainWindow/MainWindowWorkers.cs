﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Project
{
    //Workers part
    public partial class MainWindow : Window
    {
        //
        // Cводка:
        //   Коллекция сотрудников для отображения.
        //
        public ObservableCollection<A> WorkersList = new ObservableCollection<A>();

        //
        // Сводка:
        //     Вызывается другими методами этого класса в случае для блокирования/разблокирования
        //     элементов управления в интерфейсе.
        //
        // Параметры:
        //   choice:
        //     true - если требуется заблокировать интерфейс, false - если разблокировать. 
        //
        private void LockWorkersInterface(bool choice)
        {
            //сокрытие кнопки "связать".
            btnBindWorkerWithFactory.Visibility = !choice ? Visibility.Hidden : Visibility.Visible;
            
            //блокирование вкладки предприятий.
            tabFactories.IsEnabled = !choice;
            tabLinks.IsEnabled = !choice;

            //деактивация кнопок
            btnAddWorker.IsEnabled = !choice;
            btnChangeLoadWorkersDir.IsEnabled = !choice;
            btnChangeSaveWorkersDir.IsEnabled = !choice;
            btnClearWorkers.IsEnabled = !choice;
            btnLoadWorkers.IsEnabled = !choice;
            btnSaveWorkers.IsEnabled = !choice;

            //сокрытие колонки кнопок в таблице сотрудников.
            dgdWorkers.Columns[0].Visibility = !choice ? Visibility.Visible : Visibility.Hidden;

            //активация кнопки "удалить элемент" в случае, если выбрана строка.
            if (!choice && dgdWorkers.SelectedCells?.Count != 0)
                btnDeleteWorker.IsEnabled = true;
            else
                btnDeleteWorker.IsEnabled = false;

            //сдвиг шторки, содержащей таблицу связанных предприятий.
            ColumnDefinition it = grdWorkers.ColumnDefinitions[0];
            it.Width = new GridLength(!choice ? 0 : it.MaxWidth);

            //сдвиг заголовка режима блокировки интерфейса.
            grdWorkersWrap.RowDefinitions[0].Height = !choice ? new GridLength(0) : GridLength.Auto;
        }

        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "закрыть". Выход из режима добавления связи.
        //
        private void btnCloseWorkers_Click(object sender, RoutedEventArgs e)
        {
            //заблокируем элементы управления интерфейса.
            LockWorkersInterface(false);
            
            //обнулим список (не)связанных предприятий.
            dgdRelatedFactories.ItemsSource = null;

            //восстановим режим отображения таблицы.
            if (dgdRelatedFactories.Visibility == Visibility.Hidden)
            {
                //+непустая таблица, -надпись
                dgdRelatedFactories.Visibility = Visibility.Visible;
                lblNothingWorkers.Visibility = Visibility.Hidden;
            }

            //сбросим отображение списка связанных сотрудников во вкладке "предприятия".
            if (dgdFactories.SelectedCells?.Count != 0)
            {
                DataGridRow row = dgdFactories.ItemContainerGenerator.ContainerFromIndex(0) as DataGridRow;
                row.IsSelected = true;
                row.IsSelected = false;
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "искать".
        //
        private void btnSearchWorker_Click(object sender, RoutedEventArgs e)
        {
            if (WorkersList == null) return;

            //считываем значения полей из анкеты поиска сотрудника.
            string name = txtWorkerNameSearch.Text.Trim().ToLower();
            int yearFrom = (int) cmbYearFromSearch.SelectedItem;
            int yearTo = (int) cmbYearToSearch.SelectedItem;
            char gender = '\0';

            if (rdoFemaleSearch.IsChecked == true) gender = 'f';
            if (rdoMaleSearch.IsChecked == true) gender = 'm';

            //сохраним выборку элементов из списка сотрудников в соответствии с правилами выборки.
            IEnumerable<A> subcollection = from item in WorkersList
                                           where (item.Name.ToLower().Contains(name) || name.Contains(item.Name.ToLower()))
                                                 && (gender == '\0' || gender == item.Gender)
                                                 && (yearFrom <= item.Year && item.Year <= yearTo)
                                           select item;
            //обновим таблицу сотрудников.
            dgdWorkers.ItemsSource = subcollection;
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "показать все".
        //
        private void btnShowAllWorkers_Click(object sender, RoutedEventArgs e)
        {
            dgdWorkers.ItemsSource = WorkersList;
        }

        //
        // Сводка:
        //     Получает список предприятий, связанных с данным сотрудником.
        //
        // Параметры:
        //   worker:
        //     Отправной объект Project.A.
        //
        // Возврат:
        //     Список связанных предприятий.
        //
        // Исключения:
        //   T:System.ArgumentNullException:
        //     Параметр worker имеет значение null.
        //
        private List<B> GetRelatedFactories(A worker)
        {
            if (worker == null) throw new ArgumentNullException();

            List<B> relatedFactories = new List<B>();

            foreach (Link item in LinksList)
            {
                if (item.First == worker)
                {
                    relatedFactories.Add(item.Second);
                }
            }
            return relatedFactories;
        }
        //
        // Сводка:
        //     Получает список предприятий, НЕсвязанных с данным сотрудником.
        //
        // Параметры:
        //   worker:
        //     Отправной объект Project.A.
        //
        // Возврат:
        //     Список НЕсвязанных предприятий.
        //
        // Исключения:
        //   T:System.ArgumentNullException:
        //     Параметр worker имеет значение null.
        //
        private List<B> GetUnrelatedFactories(A worker)
        {
            List<B> unrelatedFactories = new List<B>();

            //получим список связанных предприятий.
            List<B> relatedFactories = GetRelatedFactories(worker);

            //вычтем его из списка предприятий.
            foreach (B item in FactoriesList)
            {
                if (!relatedFactories.Contains(item))
                    unrelatedFactories.Add(item);
            }

            return unrelatedFactories;
        }

        //
        // Сводка:
        //     Обрабатывает событие выбора строки таблицы сотрудников.
        //
        private void rowWorkerSelected(object sender, RoutedEventArgs e)
        {
            
            A worker = (A) dgdWorkers.SelectedCells[0].Item;

            //проверка на условие находится ли программа в режиме добавления связи
            
            //нейтральный режим
            if (!btnBindWorkerWithFactory.IsVisible)
            {
                //активация кнопки удаления сотрудника
                btnDeleteWorker.IsEnabled = true;

                //активация кнопки удаления связи
                if (dgdRelatedFactories.SelectedCells?.Count != 0)
                    btnDeleteLinkWithFactories.IsEnabled = true;
            }
            //режим добавления связи
            else
            {               
                //получение списка несвязанных предприятий
                var unrelatedFactories = new ObservableCollection<B>(GetUnrelatedFactories(worker));
                //привязка списка к левой таблице
                dgdRelatedFactories.ItemsSource = unrelatedFactories;

                //выбор варианта отображения таблицы (по св-ву пустой/непустой)

                //пустой
                if(unrelatedFactories.Count == 0)
                {
                    //-пустая таблица, +надпись
                    dgdRelatedFactories.Visibility = Visibility.Hidden;
                    lblNothingWorkers.Visibility = Visibility.Visible;
                }
                //непустой
                else if(dgdRelatedFactories.Visibility == Visibility.Hidden)
                {
                    //+непустая таблица, -надпись
                    dgdRelatedFactories.Visibility = Visibility.Visible;
                    lblNothingWorkers.Visibility = Visibility.Hidden;
                }

                //активация кнопки "связать" в случае, если выбраны строки в левой таблице
                if (dgdRelatedFactories.SelectedCells?.Count != 0)
                    btnBindWorkerWithFactory.IsEnabled = true;
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие выбора строки таблицы связанных предприятий.
        //
        private void rowRelatedFactorySelected(object sender, RoutedEventArgs e)
        {           
            if (dgdWorkers.SelectedCells?.Count != 0)
            {
                //активация кнопки удаления связи в обычном режиме
                if (!btnBindWorkerWithFactory.IsVisible)
                    btnDeleteLinkWithFactories.IsEnabled = true;

                //активация кнопки "связать"
                else
                    btnBindWorkerWithFactory.IsEnabled = true;
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие отмены выбора строки таблицы сотрудников.
        //
        private void rowWorkerUnselected(object sender, RoutedEventArgs e)
        {
            //деактивация кнопки "удалить элемент"
            btnDeleteWorker.IsEnabled = false;

            //деактивация кнопки "связать"
            btnBindWorkerWithFactory.IsEnabled = false;

            //деактивация кнопки "удалить связь"
            btnDeleteLinkWithFactories.IsEnabled = false;

            //обнуление таблицы связанных предприятий.
            dgdRelatedFactories.ItemsSource = null;

            //применение стандартного окраса кнопки для связанных элементов.
            if (dgdWorkers.Columns[0].Visibility == Visibility.Visible)
            {
                DataGridRow row = (DataGridRow)sender;
                GetVisualChild<Button>(row).Background = _blueBrush;
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие отмены выбора строки таблицы связанных предприятий.
        //
        private void rowRelatedFactoryUnselected(object sender, RoutedEventArgs e)
        {
            //деактивация кнопки "связать".
            btnBindWorkerWithFactory.IsEnabled = false;

            //деактивация кнопки "удалить связь".
            btnDeleteLinkWithFactories.IsEnabled = false;

        }

        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки для связанных предприятий.
        //
        private void btnWorkerExpand_Click(object sender, RoutedEventArgs e)
        {
            A worker = (A) ((Button)sender).DataContext;

            //сдвиг шторки, содержащей таблицу связанных предприятий.
            ColumnDefinition it = grdWorkers.ColumnDefinitions[0];

            if (it.ActualWidth == 0)
               it.Width = new GridLength(it.MaxWidth);

            //получим список связанных предприятий.
            var factories = new ObservableCollection<B>(GetRelatedFactories(worker));

            //смена окраса кнопки для связанных элементов.
            if(factories.Count != 0)
                ((Button)sender).Background = _greenBrush;
            else
                ((Button)sender).Background = _redBrush;

            dgdRelatedFactories.ItemsSource = factories;
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "добавить элемент".
        //
        private void btnAddWorker_Click(object sender, RoutedEventArgs e)
        {
            //создадим окно добавления сотрудника с указанием на текущее окно, откроем его.
            var win = new AddWorkerWindow(this);
            win.ShowDialog();

            //если сохраненное значение отлично от null, добавим его в список. 
            if (win.Worker != null)
                WorkersList.Add(win.Worker);

            //проверим на возможность отображения кнопок добавления связи.
            CheckForLinkButtons();
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "удалить элемент".
        //
        private void btnDeleteWorker_Click(object sender, RoutedEventArgs e)
        {
            //получим элемент из выбранной строки.
            var row = dgdWorkers.SelectedCells;
            A worker;

            if (row?.Count != 0)
                worker = (A) row[0].Item;
            else return;

            //удалим связи с этим элементом.
            LinksList.RemoveAll(item => item.First == worker);

            //удалим элемент.
            WorkersList.Remove(worker);
            dgdRelatedFactories.ItemsSource = null;

            //сбросим отображение списка связанных сотрудников во вкладке "предприятия".
            if (dgdFactories.SelectedCells?.Count != 0)
            {
                DataGridRow row1 = dgdFactories.ItemContainerGenerator.ContainerFromIndex(0) as DataGridRow;
                row1.IsSelected = true;
                row1.IsSelected = false;
            }

            //проверим на возможность отображения кнопок добавления связи.
            CheckForLinkButtons();
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "сохранить".
        //
        private void btnSaveWorkers_Click(object sender, RoutedEventArgs e)
        {
            string path = (string)btnSaveWorkers.Tag;
            string format = path.Substring(path.LastIndexOf('.'));
            ACollectionWriter writer = new ACollectionWriter();

            //хранит метку произошла ли запись успешно.
            bool flag = false;

            //обработка пустого списка
            if (WorkersList.Count == 0)
            {
                MessageBox.Show("Список пуст.");
                return;
            }
            
            //запись в текстовый файл.
            if (format == ".txt")
            {
                try
                {
                    writer.WriteToTextFile(path, WorkersList);
                    flag = true;
                }
                catch (Exception ex)
                {
                    //вывод сообщения об ошибке.
                    MessageBox.Show(ExMessageBuilder(ex), ExSourceBuilder(ex));
                    return;
                }
            }

            //запись в бинарный файл.
            if (format == ".bin" || format == ".dat")
            {
                try
                {
                    writer.WriteToBinaryFile(path, WorkersList);
                    flag = true;
                }
                catch (Exception ex)
                {
                    //вывод сообщения об ошибке.
                    MessageBox.Show(ExMessageBuilder(ex), ExSourceBuilder(ex));
                    return;
                }
            }

            //вывод сообщения об успешной записи.
            if (flag == true)
                MessageBox.Show(String.Format("Сохранено.\nПуть: {0}", path));
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "загрузить".
        //
        private void btnLoadWorkers_Click(object sender, RoutedEventArgs e)
        {
            List<A> tempA = null;
            string path = (string)btnLoadWorkers.Tag;
            string format = path.Substring(path.LastIndexOf('.'));
            ACollectionReader reader = new ACollectionReader();

            //считывание из тесктового файла.
            if (format == ".txt")
            {
                try
                {
                    tempA = reader.ReadFromTextFile(path);
                }
                catch (Exception ex)
                {
                    //вывод сообщения об ошибке.
                    MessageBox.Show(ExMessageBuilder(ex), ExSourceBuilder(ex));
                    return;
                }
            }

            //считывание из бинарного файла.
            if (format == ".bin" || format == ".dat")
            {
                try
                {
                    tempA = reader.ReadFromBinaryFile(path);
                }
                catch (Exception ex)
                {
                    //вывод сообщения об ошибке.
                    MessageBox.Show(ExMessageBuilder(ex), ExSourceBuilder(ex));
                    return;
                }
            }

            //вывод сообщения об успешном считывании.
            if (tempA?.Count != 0)
            {
                WorkersList = new ObservableCollection<A>(tempA);
                MessageBox.Show(String.Format("Загружено.\nПуть: {0}", path));
            }

            if (dgdWorkers.ItemsSource != WorkersList)
                dgdWorkers.ItemsSource = WorkersList;

            //сброс отображение списка связанных сотрудников во вкладке "предприятия".
            if (dgdFactories.SelectedCells?.Count != 0)
            {
                DataGridRow row1 = dgdFactories.ItemContainerGenerator.ContainerFromIndex(0) as DataGridRow;
                row1.IsSelected = true;
                row1.IsSelected = false;
            }

            //проверка на возможность отображения кнопок добавления связи.
            CheckForLinkButtons();

        }

        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "удалить связь".
        //
        private void btnDeleteLinkWithFactories_Click(object sender, RoutedEventArgs e)
        {
            //получим элементы из выбранных строк.
            var row1 = dgdWorkers.SelectedCells;
            var row2 = dgdRelatedFactories.SelectedCells;
            A worker = null;
            B factory = null;

            if (row1?.Count != 0)
                worker = (A)row1[0].Item;
            if (row2?.Count != 0)
                factory = (B)row2[0].Item;
            else return;

            //удалим связь (все связи).
            LinksList.RemoveAll(item => item.First == worker && item.Second == factory);

            //получим список несвязанных предприятий.
            dgdRelatedFactories.ItemsSource = new ObservableCollection<B>(GetRelatedFactories(worker));

            //сброс отображения списка связанных сотрудников во вкладке "предприятия".
            if (dgdFactories.SelectedCells?.Count != 0)
            {
                DataGridRow row3 = dgdFactories.ItemContainerGenerator.ContainerFromIndex(0) as DataGridRow;
                row3.IsSelected = true;
                row3.IsSelected = false;
            }

        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "добавить связь".
        //
        private void btnAddLinkWithFactories_Click(object sender, RoutedEventArgs e)
        {
            if (FactoriesList?.Count != 0 && WorkersList?.Count != 0)
            {
                //разблокируем элементы управления интерфейса.
                LockWorkersInterface(true);

                if (dgdWorkers.SelectedCells?.Count != 0)
                {
                    //получим список несвязанных предприятий.
                    var unrelatedFactories = new ObservableCollection<B>(GetUnrelatedFactories((A)dgdWorkers.SelectedCells[0].Item));
                    dgdRelatedFactories.ItemsSource = unrelatedFactories;

                    //смена варианта отображения таблицы.
                    if (unrelatedFactories.Count == 0)
                    {
                        //-пустая таблица, +надпись
                        dgdRelatedFactories.Visibility = Visibility.Hidden;
                        lblNothingWorkers.Visibility = Visibility.Visible;
                    }
                }
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "связать".
        //
        private void btnBindWorkerWithFactory_Click(object sender, RoutedEventArgs e)
        {
            if (dgdWorkers.SelectedCells?.Count != 0 && dgdRelatedFactories.SelectedCells?.Count != 0)
            {
                //получим элементы из выбранных строк.
                var row1 = dgdWorkers.SelectedCells;
                var row2 = dgdRelatedFactories.SelectedCells;
                A worker = null;
                B factory = null;

                if (row1?.Count != 0)
                    worker = (A)row1[0].Item;
                if (row2?.Count != 0)
                    factory = (B)row2[0].Item;
                else return;

                //добавим новую связь.
                LinksList.Add(new Link(worker, factory));

                //получим список несвязанных предприятий снова.
                var unrelatedFactories = new ObservableCollection<B>(GetUnrelatedFactories(worker));
                dgdRelatedFactories.ItemsSource = unrelatedFactories;

                //смена варианта отображения таблицы.
                if (unrelatedFactories.Count == 0)
                {
                    dgdRelatedFactories.Visibility = Visibility.Hidden;
                    lblNothingWorkers.Visibility = Visibility.Visible;
                }
                else if (dgdRelatedFactories.Visibility == Visibility.Hidden)
                {
                    dgdRelatedFactories.Visibility = Visibility.Visible;
                    lblNothingWorkers.Visibility = Visibility.Hidden;
                }
            }
        }
        //
        // Сводка:
        //     Обрабатывает событие нажатия кнопки "очистить".
        //
        private void btnClearWorkers_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult ch = MessageBox.Show("Вы уверены, что хотите очистить список сотрудников?\nВсе связи будут удалены.", "", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (ch == MessageBoxResult.Yes)
            {
                //очистим список сотрудников.
                WorkersList.Clear();

                //очистим список связей.
                LinksList.Clear();

                //проверка на возможность отображения кнопок добавления связи.
                CheckForLinkButtons();

                //сброс отображения списка связанных сотрудников во вкладке "предприятия".
                if (dgdFactories.SelectedCells?.Count != 0)
                {
                    DataGridRow row1 = dgdFactories.ItemContainerGenerator.ContainerFromIndex(0) as DataGridRow;
                    row1.IsSelected = true;
                    row1.IsSelected = false;
                }
            }
        }
    }
}
